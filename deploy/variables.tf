variable "prefix" {
  type        = string
  default     = "raad"
  description = "recipe app api devops"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "mtritterbusch@gmail.com"
}

variable "db_username" {
  description = "Username for RDS postgres instance"
}

variable "db_password" {
  description = "Password for RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "012091000722.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "012091000722.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}
